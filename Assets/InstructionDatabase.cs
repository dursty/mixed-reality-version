﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/**
* The InstructionDatabase handles sending and receiving instructions.
*
* receivers: List of GameObjects that want to get instructions from the
*            database. Must implement InstructionReceiverInterface
* arrowPrefab: The Transform to use for arrows
* sectionText: The GameObject that handles displaying the current section. Must
*               implement InstructionSectionInterface
* xmlFile: the XML to read instructions from
*/
public class InstructionDatabase : MonoBehaviour {

	public List<InstructionReceiverInterface> receivers;
	public Transform arrowPrefab;
	public InstructionSectionInterface sectionText;
	public TextAsset xmlFile;

	private InstructionSet instructionSet;
	static List<InstructionStep> currentInstructions;
	int currentSection = 0;
	int currentInstruction = 0;

	// Use this for initialization
	void Awake(){
		instructionSet = InstructionSet.loadFromFile(xmlFile.text);
		currentInstructions = instructionSet.getInstructions(currentSection);
	}

	// Called once all GameObjects are initialized
	void Start () {
		notifyReceivers(currentInstructions[0]); // Send first instruction to everyone
		sectionText.onNewSection(instructionSet.getSection(currentSection));
	}

	/**
	* Fetches the next Instruction and sends it to all receivers. If there are
	* no more instructions, the database will reset to the beginning.
	*
	* @return the Instruction that was sent
	*/
	public InstructionStep nextInstruction(){
		if(currentSection == instructionSet.getSectionCount()-1 && currentInstruction == currentInstructions.Count - 1){
			// Reset to beginning
			currentInstruction = 0;
			currentSection = 0;
			currentInstructions = instructionSet.getInstructions(currentSection);
			InstructionStep i = currentInstructions[currentInstruction];
			sectionText.onNewSection(instructionSet.getSection(currentSection));
			notifyReceivers(i);
			return i;
		}

		// End of this section
		if(currentInstruction >= currentInstructions.Count - 1){
			currentSection++;
			currentInstruction = -1; // Because this gets ++1 after

			if(currentSection < instructionSet.getSectionCount()){
				currentInstructions = instructionSet.getInstructions(currentSection);
				sectionText.onNewSection(instructionSet.getSection(currentSection));
			}else{
				// return last instruction if overflow, should handle this better
				return currentInstructions[currentInstructions.Count -1];
			}
		}

		currentInstruction++;
		InstructionStep instruction = currentInstructions[currentInstruction];
		notifyReceivers(instruction);
		return instruction;
	}

	/**
	* Fetches the previous Instruction and sends it to all receivers. If database
	* is currently at the first instruction, it will send out this instruction.
	*
	* @return the Instruction that was sent
	*/
	public InstructionStep prevInstruction(){
		if(currentInstruction == 0 && currentSection == 0){
			 // first instruction in set, can't go further back
			 return currentInstructions[0];
		}else if(currentInstruction == 0){
			currentSection--;
			currentInstructions = instructionSet.getInstructions(currentSection);
			currentInstruction = currentInstructions.Count - 1;
			sectionText.onNewSection(instructionSet.getSection(currentSection));
		}else{
			currentInstruction--;
		}

		Debug.Log(currentInstruction);
		InstructionStep instruction = currentInstructions[currentInstruction];
		notifyReceivers(instruction);
		return instruction;
	}

	/**
	* Send given instruction to all receivers.
	* @param instruction: The Instruction to send
	*/
	public void notifyReceivers(InstructionStep instruction){
		for(int i = 0;i<receivers.Count;i++){
			receivers[i].onNewInstruction(instruction);
		}
	}
}
