﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
* Simple implementation of InstructionReceiverInterface
*/
public class SampleInstructionReceiver : InstructionReceiverInterface {

    private Text txt;

    void Awake(){
        txt = GetComponent<UnityEngine.UI.Text>();
    }

	public override void onNewInstruction(InstructionStep instruction){
        txt.text = instruction.getText()[ID];
    }
}
