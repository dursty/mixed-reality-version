﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
* Interface for GameObjects that wish to recieve Instructions from the Database
*/
public abstract class InstructionReceiverInterface : MonoBehaviour {

	// Each reciever is assigned an ID
	public int ID;

	/**
	* Called when the InstructionDatabase send a new Instruction. This occurs
	* when a called to nextInstruction or prevInstruction is made
	* @param instruction: the next instruction
	*/
	public abstract void onNewInstruction(InstructionStep instruction);
}
