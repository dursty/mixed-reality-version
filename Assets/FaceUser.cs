using UnityEngine;
using System.Collections;

/**
* Simple script that keeps attached GameObject facing the User
*/
public class FaceUser : MonoBehaviour {

    public void Update(){
        this.transform.LookAt(Camera.main.transform);
        this.transform.Rotate(Vector3.up,180);
    }
}
