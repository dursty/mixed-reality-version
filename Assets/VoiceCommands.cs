﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Windows.Speech;

/**
* Handles voice commands.
*
* database: the InstructionDatabase to call next/prevInstruction
*/
public class VoiceCommands : MonoBehaviour {
    public InstructionDatabase database;
    KeywordRecognizer keywordRecognizer = null;
    Dictionary<string, System.Action> keywords = new Dictionary<string, System.Action>();


    /**
    * Called when a keyword is recognized
    * @params args: Event arguements, used to find recognized keyword
    */
    private void KeywordRecognizer_OnPhraseRecognized(PhraseRecognizedEventArgs args) {
        System.Action keywordAction;
        if (keywords.TryGetValue(args.text, out keywordAction)) {
            keywordAction.Invoke(); // This is defined in Start()
        }
    }

    void Start() {
        keywords.Add("Next",() => {
                database.nextInstruction();
        });

        keywords.Add("Back",() => {
                database.prevInstruction();
        });

        // Tell the KeywordRecognizer about our keywords.
        keywordRecognizer = new KeywordRecognizer(keywords.Keys.ToArray());

        // Register a callback for the KeywordRecognizer and start recognizing!
        keywordRecognizer.OnPhraseRecognized += KeywordRecognizer_OnPhraseRecognized;
        keywordRecognizer.Start();
    }
}
