﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Windows.Speech;

/**
* The ArrowHandler handles positioning the arrows based on the current
* instruction. It also handles switching between the outer and inner models.
*
* Arrow[1-5]: The GameObject that will represent this arrow, they should all be
*   the same model
* outer: The Mesh for the outer model
* inner: The Mesh for the inner model
*/
public class ArrowHandler : InstructionReceiverInterface{
    public GameObject Arrow1;
    public GameObject Arrow2;
    public GameObject Arrow3;
    public GameObject Arrow4;
    public GameObject Arrow5;
    public Mesh outer;
    public Mesh inner;
    private string lastModel;
    private Vector3 DefaultArrowPosition;
    private Quaternion DefaultArrowRotation;

    // Called once all GameObjects are initialized
    void Start() {
        // set the DefaultArrowPosition to the one at the beginning of the app start
        DefaultArrowPosition = Arrow1.transform.localPosition;
        DefaultArrowRotation = Arrow1.transform.localRotation;
        Debug.Log(DefaultArrowPosition);
        Debug.Log(Arrow1.transform.position);
        hideAllArrows();
        lastModel = "outer";
    }

    // Event Handler from InstructionReceiverInterface
    public override void onNewInstruction(InstructionStep instruction) {
        hideAllArrows();

        string modelType = instruction.getMainModel();
        if(modelType != null){
            if(modelType.Equals("outer")){
                if(lastModel.Equals("inner")){
                    CalButtonHandler.setShow();
                }
                gameObject.GetComponent<MeshFilter>().mesh = outer;
            } else if(modelType.Equals("inner")) {
                if(lastModel.Equals("outer")){
                    CalButtonHandler.setShow();
                }
                gameObject.GetComponent<MeshFilter>().mesh = inner;
            }
            lastModel = modelType;
        }

        List<DisplayObject> objects = instruction.getDisplayObjects();
        for(int i = 0; i < objects.Count; i++) {
            showArrow(i);
            moveArrow(i, objects[i].getTranslateCoord());
            rotateArrow(i, objects[i].getOrientation());
        }
    }

    // move Arrow i to the specified coord
    private void moveArrow(int i,Vector3 coord) {
        switch (i)
        {
            case 0:
                Arrow1.transform.localPosition = DefaultArrowPosition;
                Arrow1.transform.localRotation = DefaultArrowRotation;
                Arrow1.transform.Translate(coord);
                break;
            case 1:
                Arrow2.transform.localPosition = DefaultArrowPosition;
                Arrow2.transform.localRotation = DefaultArrowRotation;
                Arrow2.transform.Translate(coord);
                break;
            case 2:
                Arrow3.transform.localPosition = DefaultArrowPosition;
                Arrow3.transform.localRotation = DefaultArrowRotation;
                Arrow3.transform.Translate(coord);
                break;
            case 3:
                Arrow4.transform.localPosition = DefaultArrowPosition;
                Arrow4.transform.localRotation = DefaultArrowRotation;
                Arrow4.transform.Translate(coord);
                break;
            case 4:
                Arrow5.transform.localPosition = DefaultArrowPosition;
                Arrow5.transform.localRotation = DefaultArrowRotation;
                Arrow5.transform.Translate(coord);
                break;

            default:

                break;
        }
    }

    // rotate Arrow i to the specified direction
    private void rotateArrow(int i, string direction) {
        GameObject currentArrow;
        switch (i) {
            case 0:
                currentArrow = Arrow1;
                break;
            case 1:
                currentArrow = Arrow2;
                break;
            case 2:
                currentArrow = Arrow3;
                break;
            case 3:
                currentArrow = Arrow4;
                break;
            case 4:
                currentArrow = Arrow5;
                break;

            default:
                currentArrow = Arrow1;
                break;
        }

        switch (direction)
        {
            case "down":
                currentArrow.transform.localRotation = DefaultArrowRotation;
                break;
            case "right":
                currentArrow.transform.localRotation = DefaultArrowRotation;
                currentArrow.transform.rotation = Quaternion.LookRotation(gameObject.transform.forward) * Quaternion.AngleAxis(90, new Vector3(0, 0, 1));
                break;
            case "up":
                currentArrow.transform.localRotation = DefaultArrowRotation;
                currentArrow.transform.rotation = Quaternion.LookRotation(gameObject.transform.forward) * Quaternion.AngleAxis(180, new Vector3(1, 0, 0));
                break;
            case "left":
                currentArrow.transform.localRotation = DefaultArrowRotation;
                currentArrow.transform.rotation = Quaternion.LookRotation(gameObject.transform.forward) * Quaternion.AngleAxis(270, new Vector3(0, 0, 1));
                break;
            case "forward":
                currentArrow.transform.localRotation = DefaultArrowRotation;
                currentArrow.transform.rotation = Quaternion.LookRotation(gameObject.transform.forward) * Quaternion.AngleAxis(-90, new Vector3(1, 0, 0));
                break;
            case "back":
                currentArrow.transform.localRotation = DefaultArrowRotation;
                currentArrow.transform.rotation = Quaternion.LookRotation(gameObject.transform.forward) * Quaternion.AngleAxis(90, new Vector3(1, 0, 0));
                break;
            default:
                currentArrow.transform.localRotation = DefaultArrowRotation;
                break;
        }
    }

    // Hide all Arrows
    private void hideAllArrows() {
        Arrow1.GetComponent<Renderer>().enabled = false;
        Arrow2.GetComponent<Renderer>().enabled = false;
        Arrow3.GetComponent<Renderer>().enabled = false;
        Arrow4.GetComponent<Renderer>().enabled = false;
        Arrow5.GetComponent<Renderer>().enabled = false;
    }


    // Show Arrow i
    private void showArrow(int i) {
        switch (i) {
            case 0:
                Arrow1.GetComponent<Renderer>().enabled = true;
                break;
            case 1:
                Arrow2.GetComponent<Renderer>().enabled = true;
                break;
            case 2:
                Arrow3.GetComponent<Renderer>().enabled = true;
                break;
            case 3:
                Arrow4.GetComponent<Renderer>().enabled = true;
                break;
            case 4:
                Arrow5.GetComponent<Renderer>().enabled = true;
                break;

            default:
                break;
        }
    }
}
