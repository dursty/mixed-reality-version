﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;

/**
* Represents an InstructionSection in XML
*/
public class InstructionSection {
	[XmlAttribute("id")]
	public int id;

    [XmlAttribute("name")]
    public string name;

    [XmlArray("Instructions")]
 	[XmlArrayItem("Instruction")]
    public List<InstructionStep> instructions;


	/**
	* Get Instructions in this InstructionSection
	*/
    public List<InstructionStep> getInstructions(){
        return instructions;
    }

	/**
	* Get a specfic Instruction in this InstructionSection
	*/
    public InstructionStep getInstruction(int i){
        return instructions[i];
    }

	/**
	* Get this InstructionSection's id
	*/
	public int getID(){
		return id;
	}

	/**
	* Get this InstructionSection's name
	*/
    public string getName(){
        return name;
    }
}
