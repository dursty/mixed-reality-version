using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;

/**
* Represents an Vector in XML. Used for Translation and Rotation tags
*/
public class XmlVector3 {
    [XmlAttribute("x")]
    public float x;
    [XmlAttribute("y")]
    public float y;
    [XmlAttribute("z")]
    public float z;

    /**
    * Get a Vector3 version of this object
    */
    public Vector3 getVector3(){
        return new Vector3(x,y,z);
    }
}
