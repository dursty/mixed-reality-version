﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.Receivers;
using HoloToolkit.Unity.InputModule;
using UnityEngine.UI;
using HoloToolkit.Unity.InputModule.Utilities.Interactions;

/**
* Handles Pinch Input for the UI, specifically the Next,Previous, and Calibrate
* buttons.
*
* database: The InstructionDatabase to call next/prevInstruction
*/
public class MenuReciever :InteractionReceiver {
    public InstructionDatabase database;
    private CalButtonHandler handler;

    void Start(){
        GameObject model = GameObject.Find("model");
        Debug.Log("CalButton: "+GameObject.Find("CalButton").transform.GetChild(2).gameObject.GetComponent<TextMesh>());
        TextMesh CalButton = GameObject.Find("CalButton").transform.GetChild(2).gameObject.GetComponent<TextMesh>();
        handler = new CalButtonHandler(model,CalButton);
    }

    /**
    * From interface
    */
    protected override void FocusEnter(GameObject obj, PointerSpecificEventData eventData){
        Debug.Log(obj.name + " : FocusEnter");
    }

    /**
    * From interface
    */
    protected override void FocusExit(GameObject obj, PointerSpecificEventData eventData){
        Debug.Log(obj.name + " : FocusExit");
    }

    /**
    * From interface, represents a pinch gesture.
    */
    protected override void InputDown(GameObject obj, InputEventData eventData){
        Debug.Log(obj.name + " : InputDown");
        switch(obj.name){
            case "NextButton":
                database.nextInstruction();
                break;
            case "PreviousButton":
                database.prevInstruction();
                break;
            case "CalButton":
                handler.toggle();
                break;
        }
    }

    /**
    * From interface
    */
    protected override void InputUp(GameObject obj, InputEventData eventData){
        Debug.Log(obj.name + " : InputUp");
    }
}
