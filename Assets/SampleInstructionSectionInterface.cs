﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/**
* Simple implementation of InstructionSectionInterface
*/
public class SampleInstructionSectionInterface : InstructionSectionInterface {

	private Text txt;

	void Awake(){
		txt = GetComponent<UnityEngine.UI.Text>();
	}

	public override void onNewSection(InstructionSection section){
		txt.text = section.getName();
	}
}
