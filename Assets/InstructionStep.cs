﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
/**
* Represents an InstructionStep in XML
*/
public class InstructionStep {
	[XmlAttribute("id")]
	public int id;

	[XmlAttribute("mainModel")]
	public string mainModel;

	[XmlArray("Texts")]
    [XmlArrayItem("Item")]
	public List<string> text;

	[XmlArray("DisplayObjects")]
	[XmlArrayItem("DisplayObject")]
	public List<DisplayObject> DisplayObjects;

	/**
	* Get this InstructionStep's id
	*/
	public int getID(){
		return id;
	}

	/**
	* Get this InstructionStep's mainModel(outer or inner)
	*/
	public string getMainModel(){
		return mainModel;
	}

	/**
	* Get this InstructionStep's text list
	*/
	public List<string> getText(){
		return text;
	}

	/**
	* Get this InstructionStep's DisplayObjects
	*/
	public List<DisplayObject> getDisplayObjects(){
		return DisplayObjects;
	}
}
