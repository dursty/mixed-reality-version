using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.Receivers;
using HoloToolkit.Unity.InputModule;
using UnityEngine.UI;
using HoloToolkit.Unity.InputModule.Utilities.Interactions;

/**
* The CalButtonHandler handles the toggle logic for the calibrate button. It
* also updates the text and model accordingly.
*/
public class CalButtonHandler {

    private static int calCounter = 1;
    private static GameObject model;
    private static TextMesh CalButton;

    /**
    * Constructor
    * @param model: the GameObject which represents the housing model, this will
    *   be toggled between hide/show
    * @param CalButton: the TextMesh of the Calibrate button, this will be
    *   be toggled between "Hide"/"Show"
    */
    public CalButtonHandler(GameObject model, TextMesh CalButton){
        CalButtonHandler.model = model;
        CalButtonHandler.CalButton = CalButton;
    }

    /**
    * Toggle Calibrate Button. Changes state based on previous state, and
    * performs the appropriate actions.
    */
    public void toggle(){
        switch(calCounter) {
            case 0:
                //show & calibrate
                model.GetComponent<Renderer>().enabled = true;
                model.GetComponent<TwoHandManipulatable>().ManipulationMode = ManipulationMode.MoveAndRotate;
                model.GetComponent<TwoHandManipulatable>().EnableEnableOneHandedMovement = true;
                CalButton.text = "Hide";
                break;
            case 1:
                //hide
                model.GetComponent<Renderer>().enabled = false;
                model.GetComponent<TwoHandManipulatable>().ManipulationMode = ManipulationMode.None;
                model.GetComponent<TwoHandManipulatable>().EnableEnableOneHandedMovement = false;
                CalButton.text = "Show";
                break;
        }
        calCounter = (calCounter+1) % 2;
    }

    /**
    * Set Calibrate button to Show. This function should be used when you don't
    * want toggle functionality.
    */
    public static void setShow(){
        //show & calibrate
        model.GetComponent<Renderer>().enabled = true;
        model.GetComponent<TwoHandManipulatable>().ManipulationMode = ManipulationMode.MoveAndRotate;
        model.GetComponent<TwoHandManipulatable>().EnableEnableOneHandedMovement = true;
        CalButton.text = "Hide";
        calCounter = 1;
    }

    /**
    * Set Calibrate button to Hide. This function should be used when you don't
    * want toggle functionality.
    */
    public static void setHide(){
        //hide
        model.GetComponent<Renderer>().enabled = false;
        model.GetComponent<TwoHandManipulatable>().ManipulationMode = ManipulationMode.None;
        model.GetComponent<TwoHandManipulatable>().EnableEnableOneHandedMovement = false;
        CalButton.text = "Show";
        calCounter = 0;
    }
}
