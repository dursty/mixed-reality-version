﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
* Interface for GameObjects that wish to recieve Sections from the Database.
* InstructionDatabase only supports one InstructionSectionInterface at a time
*/
public abstract class InstructionSectionInterface : MonoBehaviour {

	/**
	* Called when the InstructionDatabase sends a new Section. This occurs
	* when a called to nextInstruction or prevInstruction is made, causing
	* the database to go to either the next section or the previous section.
	*
	* @param section: the next section 
	*/
	public abstract void onNewSection(InstructionSection section);
}
