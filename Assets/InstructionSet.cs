using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Xml;
using System.Xml.Serialization;

/**
* Represents an InstructionSet in XML
*/
 [XmlRoot("InstructionSet")]
public class InstructionSet {

    [XmlAttribute("name")]
    public string name;

    [XmlArray("Sections")]
 	[XmlArrayItem("Section")]
	public List<InstructionSection> sections;


    /**
    * Get a specific InstructionSection in this InstructionSet
    */
    public InstructionSection getSection(int i){
        return sections[i];
    }

    /**
    * Get Instructions from a specific InstructionSection
    */
    public List<InstructionStep> getInstructions(int i){
        return sections[i].getInstructions();
    }

    /**
    * Get this InstructionSet's name
    */
    public string getName(){
        return name;
    }

    /**
    * Get the number of sections in this InstructionSet
    */
    public int getSectionCount(){
        return sections.Count;
    }


    /**
    * Read from an XML file and parse into an InstructionSet object
    * @param filename: The name of the XML file to parse
    */
    public static InstructionSet loadFromFile(string filename){
        var serializer = new XmlSerializer(typeof(InstructionSet));
        using(var reader = new System.IO.StringReader(filename)){
             return serializer.Deserialize(reader) as InstructionSet;
        }
    }
}
